﻿using System;
using UnitTestsLimitGenerator.CLI.Wrappers;
using UnitTestsLimitGenerator.Repositories;

namespace UnitTestsLimitGenerator.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 3)
            {
                Console.WriteLine("Please provide parameters" +
                    " UnitTestsLimitGenerator.CLI.exe TestTimings.csv TestLimits.csv marginLimitPercent " +
                    @" e.g. UnitTestsLimitGenerator.CLI.exe C:\data\TestTimings.csv C:\data\TestLimits.csv 10");
                return;
            }

            int marginLimitPercent;
            try
            {
                marginLimitPercent = int.Parse(args[2]);
                if (marginLimitPercent > 100 || marginLimitPercent < 1) throw new Exception();
            }
            catch (Exception)
            {
                Console.WriteLine("Please provide marginLimitPercent as a number 1 - 100");
                return;
            }

            var repository = new TestTimingsRepository(args[0], new FileSystemWrapper());
            var generator = new TestLimitGenerator(marginLimitPercent);
            var testLimits = generator.GetLimits(repository.GetAll());
            string limitsFile = args[1];
            var persiter = new TestLimitPersister(limitsFile, new FileSystemWrapper());
            persiter.Persist(testLimits);
            Console.WriteLine($"File saved {limitsFile}");
        }
    }
}
