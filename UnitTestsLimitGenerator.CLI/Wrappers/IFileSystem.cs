﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsLimitGenerator.CLI.Wrappers
{
    public interface IFileSystem
    {
        IEnumerable<string> ReadLines(string path);
        void WriteAllLines(string file, IEnumerable<string> list);
    }
}
