﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsLimitGenerator.CLI.Wrappers
{
    public class FileSystemWrapper : IFileSystem
    {
        public IEnumerable<string> ReadLines(string path)
        {
            return File.ReadLines(path);
        }

        public void WriteAllLines(string file, IEnumerable<string> list)
        {
            File.WriteAllLines(file, list);
        }
    }
}
