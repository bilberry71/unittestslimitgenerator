﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestsLimitGenerator.Data;

namespace UnitTestsLimitGenerator
{
    public class TestLimitGenerator
    {
        private double _marginRatio;

        public TestLimitGenerator(int marginLimitPercent)
        {
            _marginRatio = (100f + marginLimitPercent) / 100f;
        }

        public IEnumerable<TestLimit> GetLimits(IEnumerable<TestTiming> testTimings)
        {
            return testTimings
                .Where(t => t.Time <= TimeSpan.FromHours(2))
                .GroupBy(t => t.Name)
                .Select(g => new TestLimit(g.Key, g.Select(t => t.GetSeconds()).Average() * _marginRatio));
        }
    }
}
