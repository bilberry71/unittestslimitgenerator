﻿using System;

namespace UnitTestsLimitGenerator.Data
{
    public class TestLimit
    {
        public TestLimit(string name, double secondsLimit)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            TimeLimit = TimeSpan.FromSeconds(secondsLimit);
        }

        public string Name { get; private set; }
        public TimeSpan TimeLimit { get; private set; }


    }
}
