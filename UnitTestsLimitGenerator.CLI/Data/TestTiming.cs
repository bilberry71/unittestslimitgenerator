﻿using System;

namespace UnitTestsLimitGenerator.Data
{
    public class TestTiming
    {
        public TestTiming(string name, TimeSpan time)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Time = time;
        }

        public string Name { get; private set; }
        public TimeSpan Time { get; private set; }

        public double GetSeconds() => Time.TotalSeconds;
    }
}
