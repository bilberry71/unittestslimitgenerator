﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestsLimitGenerator.CLI.Wrappers;
using UnitTestsLimitGenerator.Data;

namespace UnitTestsLimitGenerator.Repositories
{
    public class TestLimitPersister
    {
        public const string PERSISTER_HEADER = "Name,Limit";
        private string _limitsFile;
        private readonly IFileSystem _fileSystem;

        public TestLimitPersister(string limitsFile, IFileSystem fileSystem)
        {
            if (string.IsNullOrWhiteSpace(limitsFile))
            {
                throw new ArgumentException("No limits file to save", nameof(limitsFile));
            }
            _limitsFile = limitsFile;
            _fileSystem = fileSystem ?? throw new System.ArgumentNullException(nameof(fileSystem));
        }

        public void Persist(IEnumerable<TestLimit> testLimits)
        {
            var list = new List<string>();
            list.Add(PERSISTER_HEADER);
            list.AddRange(testLimits.Select(l => $"{l.Name},{l.TimeLimit}"));
            _fileSystem.WriteAllLines(_limitsFile, list);
        }
    }
}
