﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnitTestsLimitGenerator.CLI.Wrappers;
using UnitTestsLimitGenerator.Data;

namespace UnitTestsLimitGenerator.Repositories
{
    public class TestTimingsRepository
    {
        private string _filePath;
        private readonly IFileSystem _fileSystem;

        public TestTimingsRepository(string filePath, IFileSystem fileSystem)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException("No timings file provided", nameof(filePath));
            }

            _filePath = filePath;
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
        }

        public IEnumerable<TestTiming> GetAll()
        {
            return _fileSystem.ReadLines(_filePath)
                .Skip(1)
                .Select(l => l.Split(','))
                .Select(t => new TestTiming(t[0], TimeSpan.Parse(t[1])));
        }
    }
}
