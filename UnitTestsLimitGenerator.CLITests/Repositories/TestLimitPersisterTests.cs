﻿using UnitTestsLimitGenerator.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using UnitTestsLimitGenerator.CLITests;
using UnitTestsLimitGenerator.Data;
using System;

namespace UnitTestsLimitGenerator.Repositories.Tests
{
    [TestClass()]
    public class TestLimitPersisterTests
    {
        [TestMethod()]
        public void GivenPersister_WhenSaved_ThenProperContentIsSaved()
        {
            var fileSystem = new FileSystemStub();
            var persister = new TestLimitPersister(@"C:\test.csv", fileSystem);

            persister.Persist(new List<TestLimit>() {
                new TestLimit("t1", 1000.0d),
                new TestLimit("t2", 4000.0d),
            });

            Assert.AreEqual(@"C:\test.csv", fileSystem.WrittenFile);
            Assert.AreEqual(3, fileSystem.WrittenContents.Count());
            Assert.AreEqual(TestLimitPersister.PERSISTER_HEADER, fileSystem.WrittenContents.First());
            Assert.AreEqual("t1,00:16:40", fileSystem.WrittenContents.Skip(1).First());
            Assert.AreEqual("t2,01:06:40", fileSystem.WrittenContents.Skip(2).First());
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GivenPersister_WhenEmptyPath_ThenExceptionIsThrown()
        {
            var fileSystem = new FileSystemStub();
            var persister = new TestLimitPersister(" ", fileSystem);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GivenPersister_WhenEmptyFileSystem_ThenExceptionIsThrown()
        {
            var persister = new TestLimitPersister(@"C:\test.csv", null);
        }
    }
}