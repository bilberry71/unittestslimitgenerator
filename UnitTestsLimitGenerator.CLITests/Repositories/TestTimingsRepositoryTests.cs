﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestsLimitGenerator.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestsLimitGenerator.CLITests;

namespace UnitTestsLimitGenerator.Repositories.Tests
{
    [TestClass()]
    public class TestTimingsRepositoryTests
    {
        [TestMethod()]
        public void GivenTestTimingsRepository_WhenGetAll_ThenAllContentsIsRead()
        {
            var fileSystem = new FileSystemStub();
            var repository = new TestTimingsRepository(@"C:\test_timings.csv", fileSystem);
            var timings = repository.GetAll().ToArray();

            Assert.AreEqual(@"C:\test_timings.csv", fileSystem.ReadFile);
            Assert.AreEqual(3, timings.Length);
            Assert.AreEqual(new TimeSpan(0,16,40), timings[0].Time);
            Assert.AreEqual(new TimeSpan(1,6,40), timings[1].Time);
            Assert.AreEqual(new TimeSpan(2,2,2), timings[2].Time);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GivenTestTimingsRepository_WhenEmptyPath_ThenExceptionIsThrown()
        {
            var fileSystem = new FileSystemStub();
            var persister = new TestTimingsRepository(" ", fileSystem);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GivenTestTimingsRepository_WhenEmptyFileSystem_ThenExceptionIsThrown()
        {
            var persister = new TestTimingsRepository(@"C:\test.csv", null);
        }
    }
}