﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestsLimitGenerator.CLI.Wrappers;

namespace UnitTestsLimitGenerator.CLITests
{
    public class FileSystemStub : IFileSystem
    {
        public IEnumerable<string> WrittenContents { get; private set; }
        public string WrittenFile { get; private set; }
        public string ReadFile { get; private set; }

        public IEnumerable<string> ReadLines(string file)
        {
            ReadFile = file;
            yield return "Name,Time";
            yield return "t0,00:16:40";
            yield return "t1,01:06:40";
            yield return "t2,02:02:02";
        }

        public void WriteAllLines(string file, IEnumerable<string> list)
        {
            WrittenContents = list;
            WrittenFile = file;
        }
    }
}
