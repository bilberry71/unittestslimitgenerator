﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestsLimitGenerator.Data;

namespace UnitTestsLimitGenerator.Tests
{
    [TestClass()]
    public class TestLimitGeneratorTests
    {
        TimeSpan ts0h1kSec = new TimeSpan(0, 16, 40); //1000s
        TimeSpan ts1h4kSec = new TimeSpan(1, 6, 40); //4000s
        TimeSpan ts2h = new TimeSpan(2, 2, 2); //over 2h

        [TestMethod()]
        public void GivenGeneratorWith10PercentMargin_WhenTimeOver2Hours_ThenRemoveTimeOver2Hours()
        {
            var generator = new TestLimitGenerator(10);
            var testTimings = new List<TestTiming> {
                new TestTiming("t1", ts2h),
                new TestTiming("t1", ts1h4kSec)
                };
            var testLimits = generator.GetLimits(testTimings);
            Assert.AreEqual(1, testLimits.Count());
            Assert.AreEqual(TimeSpan.FromSeconds(4000+400), testLimits.First().TimeLimit);
        }

        [TestMethod()]
        public void GivenGeneratorWith10PercentMargin_WhenOneTestManyRecords_ThenAveragePlusMargin()
        {
            var generator = new TestLimitGenerator(10);
            var testTimings = new List<TestTiming> {
                new TestTiming("t1", ts2h),
                new TestTiming("t1", ts1h4kSec),
                new TestTiming("t1", ts0h1kSec)
                };
            var testLimits = generator.GetLimits(testTimings);
            Assert.AreEqual(1, testLimits.Count());
            Assert.AreEqual(TimeSpan.FromSeconds(2500+250), testLimits.First().TimeLimit);
        }

        [TestMethod()]
        public void GivenGeneratorWith10PercentMargin_WhenManyTestsManyRecords_ThenAveragePlusMargin()
        {
            //Arrange
            var generator = new TestLimitGenerator(10);
            var testTimings = new List<TestTiming> {
                new TestTiming("t1", ts2h),
                new TestTiming("t1", ts1h4kSec),
                new TestTiming("t1", ts0h1kSec),

                new TestTiming("t2", ts0h1kSec),
                new TestTiming("t2", ts1h4kSec),
                new TestTiming("t2", ts1h4kSec),
                new TestTiming("t2", ts2h),

                new TestTiming("t3", ts0h1kSec),
                new TestTiming("t3", ts0h1kSec)
                };

            //Act
            var testLimits = generator.GetLimits(testTimings);
            
            //Assert
            Assert.AreEqual(3, testLimits.Count());
            var t1 = testLimits.Where(t => t.Name == "t1").Single();
            var t2 = testLimits.Where(t => t.Name == "t2").Single();
            var t3 = testLimits.Where(t => t.Name == "t3").Single();
            Assert.AreEqual(TimeSpan.FromSeconds(2500 + 250), t1.TimeLimit);
            Assert.AreEqual(TimeSpan.FromSeconds(3000 + 300), t2.TimeLimit);
            Assert.AreEqual(TimeSpan.FromSeconds(1000 + 100), t3.TimeLimit);
        }
    }
}